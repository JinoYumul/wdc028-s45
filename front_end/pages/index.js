import { useState, useEffect } from 'react'
import Head from 'next/head'
import { Form, Button, Row, Col, Alert } from 'react-bootstrap'
import SalesDistributionByBrand from '../components/SalesDistributionByBrand'
import MonthlySales from '../components/MonthlySales'
import ManagerAnnualSales from '../components/ManagerAnnualSales'
import ManagersList from '../components/ManagersList'

export default function Home(){
	const fileRef = React.createRef()
	const fileRef2 = React.createRef()

	const [carsData, setCarsData] = useState([])
	const [managersData, setManagersData] = useState([])

	const toBase64 = (file) => new Promise((resolve, reject) => {
		const reader = new FileReader()
		reader.readAsDataURL(file)
		reader.onload = () => resolve(reader.result)
		reader.onerror = error => reject(error)
	})

	function uploadCarData(e){
		e.preventDefault()
		toBase64(fileRef.current.files[0])
		.then(encodedFile => {
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/cars`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({csvData: encodedFile})
			})
			.then(res => res.json())
			.then(data => {
				if(data.jsonArray.length > 0){
					setCarsData(data.jsonArray)
				}
			})
		})
	}

	function uploadManagerData(e){
		e.preventDefault()
		toBase64(fileRef2.current.files[0])
		.then(encodedFile => {
			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/managers`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({csvData: encodedFile})
			})
			.then(res => res.json())
			.then(data => {
				if(data.jsonArray.length > 0){
					setManagersData(data.jsonArray)
				}
			})
		})
	}

	return(
		<React.Fragment>
			<Head>
				<title>CSV Data Visualization</title>
			</Head>

			{carsData.length > 0
			? <Row>
				<Col xs={12} md={6}>
					<SalesDistributionByBrand rawData={carsData}/>
				</Col>
				<Col xs={12} md={6}>
					<MonthlySales rawData={carsData}/>
				</Col>
			</Row>
			: <Alert variant="info">Upload cars CSV data to see sales by brand and total monthly sales.</Alert>}

			{managersData.length > 0
			? <React.Fragment>
				<Row>
					<Col>
						<ManagerAnnualSales carData={carsData} managerData={managersData}/>
					</Col>
				</Row>
				<Row>
					<Col>
						<ManagersList managers={managersData} cars={carsData}/>
					</Col>
				</Row>
			</React.Fragment>
			: <Alert variant="info">Upload managers CSV data to see sales data sorted by brand managers.</Alert>}

			<Row>
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => uploadCarData(e)}>
						<Form.Group controlId="carUploadForm">
							<Form.Label>Upload Car Sales CSV:</Form.Label>
							<Form.Control type="file" ref={ fileRef } accept="csv" required/>
						</Form.Group>
						<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
					</Form>
				</Col>

				<Col xs={12} md={6}>
					{carsData.length === 0
					? <Alert variant="info">Upload cars CSV data to upload managers CSV data</Alert>
					: <Form onSubmit={(e) => uploadManagerData(e)}>
						<Form.Group controlId="managerUploadForm">
							<Form.Label>Upload Managers CSV:</Form.Label>
							<Form.Control type="file" ref={ fileRef2 } accept="csv" required/>
						</Form.Group>
						<Button variant="primary" type="submit" id="submitBtn2">Submit</Button>
					</Form>}
				</Col>
			</Row>

		</React.Fragment>
	)
}