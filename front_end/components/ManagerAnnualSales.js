import { useState, useEffect } from 'react'
import { HorizontalBar } from 'react-chartjs-2'

export default function ManagerAnnualSales({carData, managerData}){
	const [managers, setManagers] = useState([])
    const [brands, setBrands] = useState([])
    const [managerSales, setManagerSales] = useState([])

    useEffect(() => {
    	setManagers(managerData.map(manager => `${manager.firstName} ${manager.lastName}`))
    	setBrands(managerData.map(manager => manager.brandManaged))
    }, [managerData])

    useEffect(() => {
    	setManagerSales(brands.map(brand => {
    		let sales = 0
    		carData.forEach(element => {
    			if(element.make === brand){
    				sales = sales + parseInt(element.sales)
    			}
    		})
    		return sales
    	}))
    }, [managers])

    const data = {
    	labels: managers,
    	datasets: [
	    	{
	    		label: "Manager Annual Sales",
	    		backgroundColor: 'rgba(255,99,132,0.2)',
	    		borderColor: 'rgba(255,99,132,1)',
	    		borderWidth: 1,
	    		hoverBackgroundColor: 'rgba(255,99,132,0.4)',
	    		hoverBorderColor: 'rgba(255,99,132,1)',
	    		data: managerSales
	    	}
    	]
    }

    return(
    	<HorizontalBar data={data} />
    )
}