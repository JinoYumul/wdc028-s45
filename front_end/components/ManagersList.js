import { useState } from 'react'
import { Row, Col, ListGroup, Alert } from 'react-bootstrap'
import { Doughnut } from 'react-chartjs-2'

import {colorRandomizer} from '../helpers'

export default function ManagersList({managers, cars}){
	const [brand, setBrand] = useState('')
    const [models, setModels] = useState([])
    const [sales, setSales] = useState([])
    const [bgColors, setBgColors] = useState([])

    const computeSalesByModel = (brand) => {
    	setBrand(brand)

    	let tempModels = []
    	cars.forEach(car => {
    		let sale = 0
    		if(car.make === brand && tempModels.indexOf(car.model) === -1){
    			tempModels.push(car.model)
    		}
    	})

    	let tempSales = []
    	tempModels.forEach(model => {
    		let sale = 0
    		cars.forEach(car => {
    			if(car.model === model){
    				sale = sale + parseInt(car.sales)
    			}
    		})

    		tempSales.push(sale)
    	})

    	setModels(tempModels)
    	setSales(tempSales)
    	setBgColors(tempModels.map(() => `#${colorRandomizer()}`))
    }

    const data = {
    	labels: models,
    	datasets: [{
    		data: sales,
    		backgroundColor: bgColors,
    		hoverBackgroundColor: bgColors
    	}]
    }

    return(
    	<Row className="mt-2">
            <Col sm={3}>
                <ListGroup>
                    {managers.map(manager => {
                        return (
                            <ListGroup.Item key={manager.id} action onClick={() => computeSalesByModel(manager.brandManaged)}>
                                {manager.firstName} {manager.lastName}
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
            </Col>
            <Col sm={9}>
                {brand === ''
                ? <Alert variant="info">Choose a manager to view brand sales by car model</Alert>
                : <React.Fragment>
                    <h2 className="text-center">{brand} Sales By Car Model</h2>
                    {models.length > 0 ? <Doughnut data={data}/> : <Alert variant="info">No car sales data found</Alert>}
                </React.Fragment>}
            </Col>
        </Row>
    )
}