import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import moment from 'moment'

export default function MonthlySales({rawData}){
	const [months, setMonths] = useState([])
    const [monthlySales, setMonthlySales] = useState([])

    useEffect(() => {
    	if(rawData.length > 0){

    		let tempMonths = []

    		rawData.forEach(element => {
    			if(!tempMonths.find(month => month === moment(element.sale_date).format('MMMM'))){
    				tempMonths.push(moment(element.sale_date).format('MMMM'))
    			}
    		})

    		 const monthsRef = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    		 tempMonths.sort((a, b) => {
    		 	if(monthsRef.indexOf(a) !== -1 && monthsRef.indexOf(b) !== -1){
    		 		return monthsRef.indexOf(a) - monthsRef.indexOf(b)
    		 	}
    		 })

    		 setMonths(tempMonths)
    	}
    }, [rawData])

    useEffect(() => {
        setMonthlySales(months.map(month => {
            let sales = 0
            rawData.forEach(element => {
                if(moment(element.sale_date).format('MMMM') === month){
                    sales = sales + parseInt(element.sales)
                }
            })
            return sales
        }))
    }, [months])

    const data = {
        labels: months,
        datasets: [
            {
                label: 'Monthly Sales for the Year 2020',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: monthlySales
            }
        ]
    }

    return(
    	<Bar data={data} />
    )
}