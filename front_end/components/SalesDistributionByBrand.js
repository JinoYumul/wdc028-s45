import { useState, useEffect } from 'react'
import {Pie} from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'

export default function SalesDistributionByBrand({rawData}){
	const [brands, setBrands] = useState([])
	const [sales, setSales] = useState([])
	const [bgColors, setBgColors] = useState([])

	useEffect(() => {
		let makes = []
		rawData.forEach(element => {
			if(!makes.find(make => make === element.make)){
				makes.push(element.make)
			}
		})

		setBrands(makes)
	}, [rawData])

	useEffect(() => {
		setSales(brands.map(brand => {
			let sales = 0

			rawData.forEach(element => {
				if(element.make === brand){
					sales = sales + parseInt(element.sales)
				}
			})

			return sales
		}))

		setBgColors(brands.map(() => `#${colorRandomizer()}`))

	}, [brands])

	const data = {
		labels: brands,
		datasets: [{
			data: sales,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors
		}]
	}

	return(
		<Pie data={data}/>
	)
}