const express = require('express')
const router = express.Router()
const ManagerController = require('../controllers/managerController')

router.post('/', (req, res) => {
    ManagerController.convert(req.body).then(result => res.send(result))
})

module.exports = router